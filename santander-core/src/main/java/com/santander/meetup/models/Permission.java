package com.santander.meetup.models;

public enum Permission {
    ROLE_USER,
    ROLE_ADMIN
}
