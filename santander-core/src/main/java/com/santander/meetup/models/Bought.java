package com.santander.meetup.models;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Bought implements Serializable {
    private static final Long serialVersion = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private Long id;
    @JsonView(Views.Public.class)
    private Boolean isBought;
    @JsonView(Views.Public.class)
    @ManyToOne
    private Beer beer;
    @ManyToOne
    private Meeting meeting;
    @JsonView(Views.Public.class)
    private Integer quantity;

    public Bought() {
    }

    public Bought(Beer beer, Meeting meeting, Integer quantity, Boolean isBought) {
        this.beer = beer;
        this.meeting = meeting;
        this.quantity = quantity;
        this.isBought = isBought;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getBought() {
        return isBought;
    }

    public void setBought(Boolean bought) {
        isBought = bought;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public Meeting getMeeting() {
        return meeting;
    }

    public void setMeeting(Meeting meeting) {
        this.meeting = meeting;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
