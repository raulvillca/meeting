package com.santander.meetup.models;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Attender implements Serializable {
    private static final Long serialVersion = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @JsonView(Views.Public.class)
    @OneToOne
    private User user;
    @JsonView(Views.Public.class)
    private Boolean checkIn;

    public Attender() {
        this.checkIn = Boolean.FALSE;
    }

    public Attender(User user) {
        this.user = user;
        this.checkIn = Boolean.FALSE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Boolean checkIn) {
        this.checkIn = checkIn;
    }
}
