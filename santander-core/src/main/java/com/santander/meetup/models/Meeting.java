package com.santander.meetup.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@NamedEntityGraphs({
        @NamedEntityGraph(name = "graph.attendees", attributeNodes = @NamedAttributeNode("attendees"))
})
public class Meeting implements Serializable {
    private static final Long serialVersion = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(Views.Public.class)
    private Long id;
    @JsonView(Views.Public.class)
    private String title;
    @JsonView(Views.Public.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_AR", timezone = "America/Argentina/Buenos_Aires")
    private Date date;
    @JsonView(Views.Public.class)
    @OneToOne
    private Place place;
    @JsonView(Views.Public.class)
    @OneToOne
    private User user;
    @JsonView(Views.Public.class)
    @OneToOne(fetch = FetchType.EAGER)
    private Temp temp;
    @JsonView(Views.Public.class)
    @OneToMany
    private List<Attender> attendees;
    @JsonView(Views.Public.class)
    @OneToOne(fetch = FetchType.EAGER)
    private Bought bought;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
    }

    public List<Attender> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attender> attendees) {
        this.attendees = attendees;
    }

    public Bought getBought() {
        return bought;
    }

    public void setBought(Bought bought) {
        this.bought = bought;
    }
}
