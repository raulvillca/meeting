package com.santander.meetup.services;

import org.springframework.http.ResponseEntity;

import java.util.Date;

public interface TempService {
    ResponseEntity getWeatherByMeetingId(Long meeting_id);

    void updateMeetings(Date date);
}
