package com.santander.meetup.services;

import com.santander.meetup.dtos.WeatherDto;
import com.santander.meetup.enviroments.Development;
import com.santander.meetup.models.*;
import com.santander.meetup.repositories.BoughtRepository;
import com.santander.meetup.repositories.MeetingRepository;
import com.santander.meetup.repositories.TempRepository;
import com.santander.meetup.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@Development
@Service
public class TempServiceMock implements TempService {

    private final Logger logger = LoggerFactory.getLogger(TempServiceMock.class);
    @Autowired
    private TempRepository tempRepository;
    @Autowired
    private MeetingRepository meetingRepository;
    @Autowired
    private BoughtRepository boughtRepository;

    private RestTemplate restTemplate;
    @Value("${api.weather.url_georef}")
    private String urlWeather;
    @Value("${api.weather.url_forecast}")
    private String urlWeatherForecast;
    @Value("${api.weather.url_weather_today}")
    private String urlWeatherToday;
    @Value("${api.weather.jwt}")
    private String token;

    public ResponseEntity getWeatherByMeetingId(Long meeting_id) {
        return ResponseEntity
                .accepted()
                .body(getWeather());
    }

    private WeatherDto getWeather() {
        WeatherDto dto = new WeatherDto();
        dto.setSuccess(true);
        dto.setTemperature(24);
        dto.setPlace("Capital Federal");

        return dto;
    }

    public void updateMeetings(Date date) {
        Date dateFrom = DateUtils.truncate(date);
        Date dateTo = DateUtils.sumDays(dateFrom, 1);
        meetingRepository.findAllMeetingByDate(dateFrom, dateTo)
                .stream()
                .forEach(m -> {

                    int quantity = m.getAttendees().size();

                    if (m.getTemp() != null && m.getTemp().getDegree() < 20) {
                        quantity *= 0.7;
                    } else if (m.getTemp() != null && m.getTemp().getDegree() > 24) {
                        quantity *= 2;
                    }

                    if (m.getBought() != null && m.getBought().getQuantity() != quantity) {
                        logger.info("Actualizando cantidad de cervezas compradas");
                        Bought bought = m.getBought();
                        bought.setQuantity(quantity);
                        bought.setBought(Boolean.FALSE);
                        boughtRepository.save(bought);
                    }
                });
    }
}
