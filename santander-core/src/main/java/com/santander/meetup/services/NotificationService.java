package com.santander.meetup.services;

import com.santander.meetup.exception.BusinessException;
import com.santander.meetup.models.Meeting;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

@Service
public class NotificationService {

    private final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    @Value("mail.smtp.host")
    private String host;
    @Value("mail.smtp.port")
    private String port;
    @Value("mail.smtp.trust")
    private String trust;
    @Value("mail.smtp.username")
    private String username;
    @Value("mail.smtp.password")
    private String password;
    @Value("mail.smtp.mail")
    private String mail;

    @Autowired
    private MeetingService meetingService;

    public void sendMail(Long meeting_id) {

        logger.info("Enviando notificacion a mails - INICIO");
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", host);
        prop.put("mail.smtp.port", port);
        prop.put("mail.smtp.ssl.trust", trust);

        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Meeting meeting = meetingService.findMeeting(meeting_id)
                .orElseThrow(() -> new BusinessException("No existe meetup"));

        final String[] emails = {""};
        meeting.getAttendees().stream()
                .forEach(e -> {
                    emails[0] += e.getUser().getEmail();
                });

        Try.of(() -> {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mail));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emails[0]));
            message.setSubject("Mail Subject");

            String msg = "Enviando notificacion";

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);

            Transport.send(message);
            return message;
        }).onSuccess(message -> {
            logger.info("Enviando notificacion a mails - FIN");
        }).onFailure(t -> {
            logger.error("Ocurrio un error enviando mails", t.getMessage(), t);
            throw new BusinessException("Error enviando mails", t);
        });

    }
}
