package com.santander.meetup.services;

import com.santander.meetup.exception.AppException;
import com.santander.meetup.exception.BusinessException;
import com.santander.meetup.models.Credential;
import com.santander.meetup.models.Permission;
import com.santander.meetup.repositories.AuthRepository;
import com.santander.meetup.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class AuthService {
    private final Logger logger = LoggerFactory.getLogger(AuthService.class);

    @Autowired
    private AuthRepository authRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public User registerBasicUser(String username, String password, String email) throws BusinessException {

        authRepository.findByUsername(username).ifPresent(user -> {
            logger.error("El usuario ya existe registrado: {}", username);
            throw new BusinessException("El usuario ya existe");
        });
        Credential credential = authRepository.getCredential(Permission.ROLE_USER).get();

        return authRepository.save(new User(username, passwordEncoder.encode(password), email, Arrays.asList(credential)));
    }

    public User addPermission(String username, Permission permission) throws BusinessException {
        User user = authRepository.findByUsername(username)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando usuario: {}", username);
                    throw new AppException("El usuario no existe");
                });
        Credential credential = authRepository.getCredential(permission)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando credencial: {}", permission);
                    throw new AppException("El permiso no existe");
                });

        user.getCredential().add(credential);
        return authRepository.save(user);
    }
}
