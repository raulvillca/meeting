package com.santander.meetup.services;

import com.santander.meetup.dtos.ForecastDto;
import com.santander.meetup.dtos.ForecastResponse;
import com.santander.meetup.dtos.TemperatureDto;
import com.santander.meetup.dtos.WeatherDto;
import com.santander.meetup.enviroments.Test;
import com.santander.meetup.exception.BusinessException;
import com.santander.meetup.models.Bought;
import com.santander.meetup.models.Meeting;
import com.santander.meetup.repositories.BoughtRepository;
import com.santander.meetup.repositories.MeetingRepository;
import com.santander.meetup.repositories.TempRepository;
import com.santander.meetup.utils.DateUtils;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Test
@Service
public class TempServiceImpl implements TempService {

    private final Logger logger = LoggerFactory.getLogger(TempServiceImpl.class);
    @Autowired
    private TempRepository tempRepository;
    @Autowired
    private MeetingRepository meetingRepository;
    @Autowired
    private BoughtRepository boughtRepository;

    private RestTemplate restTemplate;
    @Value("${api.weather.url_georef}")
    private String urlWeather;
    @Value("${api.weather.url_forecast}")
    private String urlWeatherForecast;
    @Value("${api.weather.url_weather_today}")
    private String urlWeatherToday;
    @Value("${api.weather.jwt}")
    private String token;

    public ResponseEntity getWeatherByMeetingId(Long meeting_id) {
        return ResponseEntity
                .accepted()
                .body(getWeather(meetingRepository.findMeeting(meeting_id)
                        .<BusinessException>orElseThrow(() -> new BusinessException("No se encuentra el meeting"))));
    }

    private Try<WeatherDto> getWeather(Meeting meeting) {
        WeatherDto dto = new WeatherDto();
        restTemplate = new RestTemplate();

        return Try.of(() -> {
            List<Object> georef = restTemplate.getForObject(urlWeather,
                    List.class,
                    meeting.getPlace().getPlace(),
                    meeting.getPlace().getPlace());

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", token);
            List<Object> objects = (List<Object>) georef.get(0);
            Integer id = Integer.parseInt(objects.get(0) + "");
            HttpEntity request = new HttpEntity(headers);

            logger.info("Consultando fecha de hoy");
            ResponseEntity<TemperatureDto> tempResponse = restTemplate.exchange(urlWeatherToday,
                    HttpMethod.GET,
                    request,
                    TemperatureDto.class,
                    id);

            Date dateMeeting = DateUtils.truncate(meeting.getDate());
            Date dateToday = DateUtils.truncate(new Date());

            if (tempResponse.hasBody() && dateMeeting.equals(dateToday)) {
                dto.setTemperature(tempResponse.getBody().getTemperature().intValue());
                dto.setPlace(meeting.getPlace().getPlace());
                dto.setSuccess(true);
                return dto;
            }

            logger.info("Consultando fechas posteriores");
            ResponseEntity<ForecastResponse> forecastResponse = restTemplate.exchange(urlWeatherForecast,
                    HttpMethod.GET,
                    request,
                    ForecastResponse.class,
                    id);

            logger.info("Comparando proximos dias");
            Date finalDateMeeting = dateMeeting;
            Optional<ForecastDto> forecastDto = forecastResponse.getBody().getForecast()
                    .stream()
                    .filter(t -> finalDateMeeting.equals(t.getDate()))
                    .findFirst();

            if (forecastDto.isPresent()) {
                dto.setSuccess(true);
                dto.setPlace(meeting.getPlace().getPlace());
                dto.setTemperature(forecastDto.get().getTemp_max());
            }

            return dto;
        });
    }

    public void updateMeetings(Date date) {
        Date dateFrom = DateUtils.truncate(date);
        Date dateTo = DateUtils.sumDays(dateFrom, 1);
        meetingRepository.findAllMeetingByDate(dateFrom, dateTo)
                .stream()
                .forEach(m -> {

                    getWeather(m).onSuccess(dto -> {
                        logger.info("Actualizando temperatura");
                        Optional.of(m.getTemp()).ifPresent(temp -> {
                            this.tempRepository.findById(temp.getId());
                            temp.setDegree(dto.getTemperature());
                            this.tempRepository.save(temp);
                        });
                    }).onFailure(t -> {
                        logger.error("Error consultando el clima {}", t.getMessage(), t);
                        throw new BusinessException("Error tratando de consultar api de clima");
                    });

                    int quantity = m.getAttendees().size();

                    if (m.getTemp() != null && m.getTemp().getDegree() < 20) {
                        quantity *= 0.7;
                    } else if (m.getTemp() != null && m.getTemp().getDegree() > 24) {
                        quantity *= 2;
                    }

                    int finalQuantity = quantity;
                    Optional.of(m.getBought()).ifPresent(b -> {
                        logger.info("Actualizando cantidad de cervezas compradas");
                        if (m.getBought().getQuantity() != finalQuantity) {
                            Bought bought = m.getBought();
                            bought.setQuantity(finalQuantity);
                            bought.setBought(Boolean.FALSE);
                            boughtRepository.save(bought);
                        }
                    });
                });
    }
}
