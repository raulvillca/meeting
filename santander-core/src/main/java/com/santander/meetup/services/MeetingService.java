package com.santander.meetup.services;

import com.santander.meetup.exception.BusinessException;
import com.santander.meetup.models.*;
import com.santander.meetup.repositories.*;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MeetingService {
    private final Logger logger = LoggerFactory.getLogger(MeetingService.class);

    @Autowired
    private AuthRepository authRepository;
    @Autowired
    private MeetingRepository meetingRepository;
    @Autowired
    private BeerRepository beerRepository;
    @Autowired
    private BoughtRepository boughtRepository;
    @Autowired
    private TempRepository tempRepository;
    @Autowired
    private PlaceRepository placeRepository;
    @Autowired
    private AttenderRepository attenderRepository;

    public List<Meeting> findAllByUser(String name) {
        return meetingRepository.findMeetingsByUsername(name);
    }

    public Page<Meeting> findAllMyMeetings(String name, Pageable pageable) {
        return meetingRepository.findMyMeetings(name, pageable);
    }

    public Optional<Meeting> findMeeting(Long id) {
        return meetingRepository.findMeeting(id);
    }

    public Integer getBeerPacks(Long id) {
        final Integer[] quantityBeers = {0};
        Try.of(() -> meetingRepository.getBeerQuantityByMeetingId(id))
                .onSuccess(quantity -> {
                    quantityBeers[0] = quantity % 6 <= 3 ? (quantity / 6) + 1 : (quantity / 6);
                    logger.error("Cantidad de cajas de cervezas {}", quantityBeers[0]);
                })
                .onFailure(t -> {
                    logger.error("Error ");
                    Meeting meeting = meetingRepository.findMeeting(id)
                            .<BusinessException>orElseThrow(() -> {
                                logger.error("Error No existe meetup para este id : {}", id);
                                throw new BusinessException("No existe meetup");
                            });
                    quantityBeers[0] = meeting.getAttendees().size();
                });
        return quantityBeers[0];
    }

    public Meeting create(Date date, Long placeId, String name, String title) throws BusinessException {
        logger.info("Creando meetup - INICIO");
        User user = authRepository.findByUsername(name)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando el usuario: {}", name);
                    throw new BusinessException("No existe el usuario");
                });

        Place place = placeRepository.findById(placeId)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando el lugar - id: {}", placeId);
                    throw new BusinessException("No existe lugar para el evento");
                });

        Attender attender = attenderRepository.save(new Attender(user));

        Meeting meeting = new Meeting();
        meeting.setUser(user);
        meeting.setDate(date);
        meeting.setTitle(title);
        meeting.setPlace(place);
        meeting.setAttendees(Arrays.asList(attender));

        logger.info("Creando meetup - FIN");
        return meetingRepository.save(meeting);
    }

    public Boolean addBeers(Long meeting_id, Long beer_id) {
        logger.info("Agregando cervezas - INICIO");
        Meeting meeting = meetingRepository.findMeeting(meeting_id)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando el meetup - id: {}", meeting_id);
                    throw new BusinessException("No existe meetup");
                });

        Beer beer = beerRepository.findById(beer_id)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando la cerveza - id: {}", beer_id);
                    throw new BusinessException("No existe la cerveza que quiere agregar");
                });

        Temp temp = meeting.getTemp();
        int number_beers = meeting.getAttendees().size();

        if (temp != null && temp.getDegree() < 20) {
            number_beers *= 0.7;
        } else if (temp != null && temp.getDegree() > 24){
            number_beers *= 2;
        }

        meeting.setBought(boughtRepository.save(new Bought(beer, meeting, number_beers, Boolean.TRUE)));

        logger.info("Agregando cervezas - FIN");
        return Boolean.TRUE;
    }

    public Boolean joinEvent(String username, Long id) {
        Meeting meeting = meetingRepository.findById(id)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando el meetup - id {}", id);
                    throw new BusinessException("No existe meetup");
                });

        User user = authRepository.findByUsername(username)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando el usuario: {}", username);
                    throw new BusinessException("No existe usuario");
                });

        Attender attender = attenderRepository.save(new Attender(user));
        meeting.getAttendees().add(attender);
        meetingRepository.save(meeting);

        return Boolean.TRUE;
    }

    public Boolean checkInEvent(String username, Long meeting_id) {
        Meeting meeting = meetingRepository.findMeeting(meeting_id)
                .<BusinessException>orElseThrow(() -> {
                    logger.error("Error consultando el usuario : {}", username);
                    throw new BusinessException("No existe meeting");
                });
        List<Attender> attenders = meeting.getAttendees().stream()
                .filter(a -> a.getUser().getUsername().equals(username))
                .map(MeetingService::setCheck)
                .collect(Collectors.toList());

        attenderRepository.saveAll(attenders);

        return Boolean.TRUE;
    }

    public Page<Meeting> findAllMeetings(Pageable pageable) {
        return meetingRepository.findAllMeetings(pageable);
    }

    public static Attender setCheck(Attender attender) {
        attender.setCheckIn(true);
        return attender;
    }

    public List<Place> findAllPlaces() {
        return placeRepository.findAll();
    }

    public List<Beer> findAllBeers() {
        return beerRepository.findAll();
    }
}
