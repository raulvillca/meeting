package com.santander.meetup.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);
    public static Date truncate(Date date) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(sdf.format(date));
        } catch (Exception e) {
            logger.error("No se puede parsear la fecha. ", e.getMessage());
        }

        return date;
    }

    public static Date sumDays(Date date, Integer quantity){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, quantity);
        return calendar.getTime();
    }
}
