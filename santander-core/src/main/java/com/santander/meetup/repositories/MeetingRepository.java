package com.santander.meetup.repositories;

import com.santander.meetup.models.Meeting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface MeetingRepository extends JpaRepository<Meeting, Long> {

    @Query("Select m From Meeting m Join m.attendees a Where a.user.username = :username")
    List<Meeting> findMeetingsByUsername(@Param("username") String username);

    @EntityGraph(value = "graph.attendees", type = EntityGraph.EntityGraphType.LOAD)
    @Query("Select m From Meeting m Where m.id = :id")
    Optional<Meeting> findMeeting(@Param("id") Long id);

    @Query("Select m.bought.quantity From Meeting m Where m.id = :id")
    Integer getBeerQuantityByMeetingId(@Param("id") Long id);

    @Query("Select m From Meeting m Where m.user.username = :username Order By m.date Desc")
    Page<Meeting> findMyMeetings(@Param("username") String username, Pageable pageable);

    @EntityGraph(value = "graph.attendees", type = EntityGraph.EntityGraphType.LOAD)
    @Query("Select m From Meeting m Where m.date Between :from AND :to")
    List<Meeting> findAllMeetingByDate(@Param("from") Date from, @Param("to") Date to);

    @Query("Select m From Meeting m Order By m.date Desc")
    Page<Meeting> findAllMeetings(Pageable pageable);
}
