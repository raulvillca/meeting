package com.santander.meetup.repositories;

import com.santander.meetup.models.Credential;
import com.santander.meetup.models.Permission;
import com.santander.meetup.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<User, Long> {

    @Query("Select u From User u Where u.username = :username")
    Optional<User> findByUsername(@Param("username") String username);

    @Query("Select c From Credential c Where c.permission = :permission")
    Optional<Credential> getCredential(@Param("permission") Permission permission);
}
