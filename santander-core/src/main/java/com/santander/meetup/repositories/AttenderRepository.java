package com.santander.meetup.repositories;

import com.santander.meetup.models.Attender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttenderRepository extends JpaRepository<Attender, Long> {
}
