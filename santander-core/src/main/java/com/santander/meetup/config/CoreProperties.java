package com.santander.meetup.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:weather.properties")
@PropertySource("classpath:mail.properties")
public class CoreProperties {
    public CoreProperties() {
        super();
    }
}
