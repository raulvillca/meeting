package com.santander.meetup.dtos;

public class WeatherDto {
    private Integer temperature;
    private String place;
    private Boolean success;

    public WeatherDto() {
        this.success = Boolean.FALSE;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
