package com.santander.meetup.dtos;

import java.util.Date;

public class TemperatureDto {
    private Date date;
    private Double temperature;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }
}
