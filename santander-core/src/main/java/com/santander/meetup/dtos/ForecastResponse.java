package com.santander.meetup.dtos;

import java.util.List;

public class ForecastResponse {
    private List<ForecastDto> forecast;

    public List<ForecastDto> getForecast() {
        return forecast;
    }

    public void setForecast(List<ForecastDto> forecast) {
        this.forecast = forecast;
    }
}
