**Api Santander Meetup**

En Santander Tecnología queremos armar las mejores meetups y para eso estamos planeando
hacer una app que nos ayude a lograr que no falte lo fundamental... birras!
Tenemos un proveedor que nos vende cajas de birras de 6 unidades. El problema es que si
hace entre 20 y 24 grados se toma una birra, si hace menos de 20 grados se toma 0.75 birras
por persona y si hace calor (más de 24 grados) se toman 2 birras más por persona, y siempre
es preferible que sobre a que falte. 

---

EL proyecto posee seguridad oauth2 y con envio de mails.
Se crearon test de ejecucion y servicios mocks para consultar el clima.

---

## Version
**Paquetes Backend**

1. Spring Boot version **2.2.5.RELEASE**
2. Spring Security OAuth2 version **2.3.4.RELEASE**
3. Jwt version **1.0.9.RELEASE**
4. javax.mail version **1.5.0-b01**

**Paquetes Frontend**

1. Angular version **7.1.4**
2. Bootstrap version **4.4.1**
3. Sweetalert2 version **9.10.2*

---

## Endpoints del sistema

**Ambientes Locales**
1. url de la api: http://localhost:8080/santander-meeting
2. url de la app: http://localhost:4200

**Ambientes Productivos**

1. url de la api: https://santander-meetup.herokuapp.com
2. url de la app: https://santander-meetup.firebaseapp.com

---

## Compilar y Ejecutar

Tener instalado Maven y NgClient

**Para entornos locales**

1. Modificar en el archivo application.properties el puerto del servidor a:
    > server.port=8080
2. Ejecutar desde el modulo **create-deploy**:
    > mvn clean install
2. Ejecutar:
    > java -jar santander-meetup-2.0.jar
3. Ejecutar desde el modulo **santander-web**:
    > ng serve

**Para entornos productivos**
* Tener cuentas en heroku y firebase

Despliegue en Heroku:

1. Conectarse a heroku: *heroku login*
2. Ejecutar desde el modulo **create-deploy**:
    > mvn clean packege
3. Ejecutar:
    > heroku git:remote -a santander-meetup
    1. Si no se tiene instalado java en heroku, hacer: **heroku plugins:install java**
4. Desde el modulo **santander-meetup** ejecutar:

    > heroku jar:deploy ./target/santander-meetup-2.0.jar

Despliegue en firebase

1. Hacer desde el modulo **santander-web**:
    > ng build --prod
2. Renombrar del directorio **/dist** el nombre del proyecto generado (santander-meetup) a **public**
3. Moverse al directorio **/dist** 
4. Crear desde la consola de firebase (pagina), un nuevo proyecto.
5. Ejecutar:
    > firebase login
6. Luego de loguearse en firebase, ejecutar:
    > firebase init
    1. Mantener la carpeta por defecto (public)
    2. Seleccionar la opcion para hosting
    3. No sobreescribir index.html, ni 404.html
7. Ejecutar: 
    > firebase user --add
    1. Seleccionar la opcion donde este nuestro proyecto creado en la consola de firebase(pagina)
    2. Crear un alias
8. Ejecutar: 
    > firebase deploy

---