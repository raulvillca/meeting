package com.santander.meetup;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.santander.meetup.controllers.AdminController;
import com.santander.meetup.controllers.MeetingController;
import com.santander.meetup.dtos.MeetingDto;
import com.santander.meetup.dtos.UserDto;
import com.santander.meetup.repositories.AuthRepository;
import org.junit.Before;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = SantanderMeetupApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SantanderMeetupApplicationTests {

    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private FilterChainProxy springSecurityFilterChain;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private MockMvc mvc;

    @Mock
    private AdminController adminController;
    @Mock
    private MeetingController meetingController;
    @Autowired
    private AuthRepository authRepository;
    @Before
    public void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(wac)
                .apply(springSecurity(springSecurityFilterChain))
                .build();

    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @Order(1)
    void register() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setUsername("suser");
        userDto.setPassword("suser");
        userDto.setEmail("suser@gmail.com");
        ResultActions resultActions = mvc.perform(post("/oauth/register")
                .header("Authorization", "Basic " + new String(Base64Utils.encode(("santander:JWTSantanderSecretKey").getBytes())))
                .content(mapToJson(userDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        resultActions.andExpect(status().is(202));
    }

    @Test
    @Order(2)
    void login() throws Exception {
        ResultActions resultActions = mvc.perform(post("/oauth/token")
                .header("Authorization", "Basic " + new String(Base64Utils.encode(("santander:JWTSantanderSecretKey").getBytes())))
                .param("username", "suser")
                .param("password", "suser")
                .param("grant_type", "password"))
                .andDo(print());

        resultActions.andExpect(status().isOk());
    }


    @Test
    @Order(3)
    void createMeetingTest() throws Exception {

        MeetingDto meetingDto = new MeetingDto();
        meetingDto.setTitle("Evento Test");
        meetingDto.setPlaceId(1L);
        meetingDto.setDate(new Date());

        ResultActions resultActions = mvc.perform(post("/admin/create_event")
                .header(HttpHeaders.AUTHORIZATION, obtainAccessToken("suser", "suser"))
                .content(mapToJson(meetingDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        resultActions.andExpect(status().is4xxClientError());
    }

    @Test
    @Order(4)
    void signUpEventTest() throws Exception {
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.put("/meetings/join_event/{id}", 1)
                .header(HttpHeaders.AUTHORIZATION, obtainAccessToken("suser", "suser"))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        resultActions.andExpect(status().is4xxClientError());
    }

    @Test
    @Order(5)
    void doCheckInTest() throws Exception {
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.put("/meetings/check_id/{id}", 1)
                .header(HttpHeaders.AUTHORIZATION, obtainAccessToken("suser", "suser"))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        resultActions.andExpect(status().is4xxClientError());
    }

    @Test
    @Order(6)
    void countBeerPacksTest() throws Exception {

        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.get("/admin/count_beer_packs/{id}", 1)
                .header(HttpHeaders.AUTHORIZATION, obtainAccessToken("suser", "suser"))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        resultActions.andExpect(status().is4xxClientError());
    }

    private String obtainAccessToken(String username, String password) throws Exception {

        ResultActions resultActions = mvc.perform(post("/oauth/token")
                .header("Authorization", "Basic " + new String(Base64Utils.encode(("santander:JWTSantanderSecretKey").getBytes())))
                .param("username", username)
                .param("password", password)
                .param("grant_type", "password"))
                .andDo(print());

        String resultString = resultActions.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();

        String token = "Baerer " + jsonParser.parseMap(resultString).get("access_token").toString();
        return token;
    }
}
