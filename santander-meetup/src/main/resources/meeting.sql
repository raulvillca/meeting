----USE meeting;


INSERT INTO Credential VALUES (1, 'ROLE_USER'), (2, 'ROLE_ADMIN');
INSERT INTO Beer VALUES (1, 'Quilmes', 100), (2, 'Brahma', 100), (3, 'Estella', 100);
INSERT INTO Place VALUES (1, 'Capital Federa', 'Calle falsa 123'), (2, 'Ramos Mejia', 'Calle super falsa 234'), (3, 'La plata', 'Calle mas falsa 123');
