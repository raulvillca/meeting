package com.santander.meetup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SantanderMeetupApplication {

	public static void main(String[] args) {
		SpringApplication.run(SantanderMeetupApplication.class, args);
	}
}
