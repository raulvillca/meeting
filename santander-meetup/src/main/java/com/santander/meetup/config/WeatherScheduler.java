package com.santander.meetup.config;

import com.santander.meetup.services.TempService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;

@Component("weatherScheduler")
public class WeatherScheduler {
    private final Logger logger = LoggerFactory.getLogger(WeatherScheduler.class);

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    private TempService tempService;
    @Value("${job.delay.milliseconds}")
    private Long timeDelay;

    @PostConstruct
    public void scheduleRunnableWithCronTrigger() {
        taskScheduler.scheduleWithFixedDelay(new RunnableTask(tempService), timeDelay);
        logger.info("Contruyendo crone");
    }

    class RunnableTask implements Runnable {

        private TempService tempService;

        public RunnableTask(TempService tempService) {
            this.tempService = tempService;
        }

        @Override
        public void run() {
            logger.info("Consultando Clima - INICIO");
            tempService.updateMeetings(new Date());
            logger.info("Consultando Clima - FIN");
        }
    }
}
