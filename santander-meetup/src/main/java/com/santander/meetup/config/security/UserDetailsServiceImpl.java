package com.santander.meetup.config.security;

import com.santander.meetup.models.User;
import com.santander.meetup.repositories.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final Map<String, UserDetailsImpl> users = new HashMap<>();

    @Autowired
    private AuthRepository authRepository;

    public UserDetailsServiceImpl() {
        UserDetailsImpl user = new UserDetailsImpl(
                1L,
                "suser",
                "suser",
                true,
                true,
                true,
                true,
                Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"))
        );
        users.put(user.getUsername(), user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetailsImpl userDetails = null;

        User user = authRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Usuario no existe"));

        List<GrantedAuthority> authorities = user.getCredential().stream()
                .map(c -> new SimpleGrantedAuthority(c.getPermission().name()))
                .collect(Collectors.toList());

        userDetails = new UserDetailsImpl(user.getId(),
                user.getUsername(),
                user.getPassword(),
                true,
                true,
                true,
                true,
                authorities);

        return userDetails;
    }

}