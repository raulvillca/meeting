package com.santander.meetup.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.santander.meetup.models.Views;
import com.santander.meetup.services.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@Secured("ROLE_USER")
@RequestMapping("/meetings")
public class MeetingController {

    @Autowired
    private MeetingService meetingService;

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/join_event/{id}", method = RequestMethod.PUT)
    public ResponseEntity joinEvent(@PathVariable Long id, Principal principal) {
        return ResponseEntity.accepted()
                .body(meetingService.joinEvent(principal.getName(), id));
    }

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/check_in/{meeting_id}", method = RequestMethod.PUT)
    public ResponseEntity checkIn(Principal principal, @PathVariable Long meeting_id) {
        return ResponseEntity.accepted()
                .body(meetingService.checkInEvent(principal.getName(), meeting_id));
    }

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/get_meeting/{id}", method = RequestMethod.GET)
    public ResponseEntity getMeeting(Principal principal, @PathVariable Long id) {
        return ResponseEntity
                .accepted()
                .body(meetingService.findMeeting(id));
    }

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/all_myevents", method = RequestMethod.GET)
    public ResponseEntity allMyEvents(Principal principal) {
        return ResponseEntity
                .accepted()
                .body(meetingService.findAllByUser(principal.getName()));
    }

    @RequestMapping(value = "/all_meetings", method = RequestMethod.GET)
    public ResponseEntity allMeetings(Principal principal, Pageable pageable) {
        return ResponseEntity
                .accepted()
                .body(meetingService.findAllMeetings(pageable));
    }

    @RequestMapping(value = "/count_beer_packs/{id}", method = RequestMethod.GET)
    public ResponseEntity countBeerPacks(@PathVariable Long id) {
        return ResponseEntity
                .accepted()
                .body(meetingService.getBeerPacks(id));
    }
}
