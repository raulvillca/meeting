package com.santander.meetup.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.santander.meetup.dtos.UserDto;
import com.santander.meetup.models.Permission;
import com.santander.meetup.models.Views;
import com.santander.meetup.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/oauth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    ResponseEntity register(@RequestBody UserDto user, Principal principal) {
        return ResponseEntity.accepted()
                .body(authService.registerBasicUser(user.getUsername(), user.getPassword(), user.getEmail()));
    }

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/set_admin", method = RequestMethod.POST)
    ResponseEntity setAdmin(@RequestBody UserDto userDto) {
        return ResponseEntity.accepted()
                .body(authService.addPermission(userDto.getUsername(), Permission.ROLE_ADMIN));
    }
}
