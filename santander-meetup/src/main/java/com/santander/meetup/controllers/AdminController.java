package com.santander.meetup.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.santander.meetup.dtos.MeetingDto;
import com.santander.meetup.models.Views;
import com.santander.meetup.services.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@Secured("ROLE_ADMIN")
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private MeetingService meetingService;

    @JsonView(Views.Public.class)
    @RequestMapping(value = "/create_event", method = RequestMethod.POST)
    public ResponseEntity createEvent(@RequestBody MeetingDto meetingDto, Principal principal) {
        return ResponseEntity
                .accepted()
                .body(meetingService.create(meetingDto.getDate(), meetingDto.getPlaceId(), principal.getName(), meetingDto.getTitle()));
    }

    @RequestMapping(value = "/get_beerpack/{id}", method = RequestMethod.GET)
    public ResponseEntity getBeerPack(Principal principal, @PathVariable Long id) {
        return ResponseEntity
                .accepted()
                .body(meetingService.getBeerPacks(id));
    }

    @RequestMapping(value = "/add_beer/{meeting_id}/{beer_id}", method = RequestMethod.GET)
    public ResponseEntity addBeer(Principal principal, @PathVariable Long meeting_id, @PathVariable Long beer_id) {
        return ResponseEntity
                .accepted()
                .body(meetingService.addBeers(meeting_id, beer_id));
    }

    @RequestMapping(value = "/all_places", method = RequestMethod.GET)
    public ResponseEntity allPlaces() {
        return ResponseEntity
                .accepted()
                .body(meetingService.findAllPlaces());
    }

    @RequestMapping(value = "/all_beers", method = RequestMethod.GET)
    public ResponseEntity allBeers() {
        return ResponseEntity
                .accepted()
                .body(meetingService.findAllBeers());
    }

    @RequestMapping(value = "/all_mymeetings", method = RequestMethod.GET)
    public ResponseEntity allMyMeetings(Principal principal, Pageable pageable) {
        return ResponseEntity
                .accepted()
                .body(meetingService.findAllMyMeetings(principal.getName(), pageable));
    }
}
