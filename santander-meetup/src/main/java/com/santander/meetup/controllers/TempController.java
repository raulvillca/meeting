package com.santander.meetup.controllers;

import com.santander.meetup.services.TempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@Secured("ROLE_USER")
@RestController
@RequestMapping("/temps")
public class TempController {
    @Autowired
    private TempService tempService;

    @RequestMapping(value = "/get_weather/{meeting_id}", method = RequestMethod.GET)
    public ResponseEntity getWeather(@PathVariable Long meeting_id) {
        return ResponseEntity
                .accepted()
                .body(tempService.getWeatherByMeetingId(meeting_id));
    }
}
