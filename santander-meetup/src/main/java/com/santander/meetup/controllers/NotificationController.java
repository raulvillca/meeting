package com.santander.meetup.controllers;

import com.santander.meetup.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/notifications")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @RequestMapping(value = "/send_notification/{meeting_id}", method = RequestMethod.POST)
    ResponseEntity sendNotification(@PathVariable Long meeting_id) {
        notificationService.sendMail(meeting_id);
        return ResponseEntity.accepted()
                .body("ok");
    }
}
