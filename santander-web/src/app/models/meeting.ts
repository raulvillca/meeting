import { Attender } from './attender';
import { Place } from './place';
import { User } from './user';
import { Bought } from './bought';

export class Meeting {
    id: number;
    date: string;
    place: Place;
    user: User;
    attendees: Attender[];
    bought: Bought;
}
