import { Beer } from './beer';

export class Bought {
    id: number;
    beer: Beer;
    quantity: number;
}
