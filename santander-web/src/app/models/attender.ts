import { User } from './user';

export class Attender {
    id: number;
    user: User;
    checkin: boolean;
}
