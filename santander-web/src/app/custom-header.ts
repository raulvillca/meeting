import { HttpHeaders } from '@angular/common/http';

export class CustomHeader {
    constructor() {
    }

    static build() : HttpHeaders {
        return new HttpHeaders({
            'Content-type':'application/json',
            'Authorization': 'Bearer ' + sessionStorage.getItem('token')
          });
    }
}
