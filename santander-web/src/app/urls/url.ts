export const URL_SERVER: string = "https://santander-meetup.herokuapp.com";
export const URL_AUTH : string = URL_SERVER + "/santander-meeting/oauth";
export const URL_LOGIN : string = URL_AUTH + "/token";
export const URL_REGISTER : string = URL_AUTH + "/register";

export const URL_MEETING : string = URL_SERVER + "/santander-meeting/meetings";
export const URL_MEETING_JOIN : string = URL_MEETING + "/join_event/";
export const URL_MEETING_CHECKIN : string = URL_MEETING + "/check_in/";
export const URL_MEETING_MY_MEETING : string = URL_MEETING + "/get_meeting/";
export const URL_MEETING_MY_EVENTS : string = URL_MEETING + "/all_myevents";
export const URL_MEETING_ALL_MEETINGS : string = URL_MEETING + "/all_meetings";

export const URL_ADMIN : string = URL_SERVER + "/santander-meeting/admin";
export const URL_MEETING_CREATE : string = URL_ADMIN + "/create_event";
export const URL_MEETING_MY_MEETINGS : string = URL_ADMIN + "/all_mymeetings";
export const URL_MEETING_ALL_PLACES : string = URL_ADMIN + "/all_places";
export const URL_MEETING_ALL_BEERS : string = URL_ADMIN + "/all_beers";
export const URL_MEETING_BEER_PACK : string = URL_ADMIN + "/get_beerpack/";


export const URL_TEMP_GET_WEATHER : string = URL_SERVER + "/santander-meeting/temps/get_weather/";
export const URL_NOTIFICATION_SEND : string = URL_SERVER + "/santander-meeting/notifications/send";