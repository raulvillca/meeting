import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { RouterModule, Routes} from '@angular/router';
import { HeaderComponent } from './components/utils/header/header.component';
import { FooterComponent } from './components/utils/footer/footer.component';
import { MeetingComponent } from './components/meetingPage/meeting/meeting.component';
import { CardComponent } from './components/meetingPage/card/card.component'
import { AdminComponent } from './components/adminPage/admin/admin.component';
import { LoginComponent } from './components/loginPage/login/login.component';
import { RegisterComponent } from './components/loginPage/register/register.component';
import { AuthService } from './services/loginPage/auth.service';
import { MeetingService } from './services/meetingPage/meeting.service';
import { PaginationComponent } from './components/utils/pagination/pagination.component';
import { ModelComponent } from './components/utils/model/model.component';
import { AuthGuard } from './services/guards/auth.guard';
import { RoleGuard } from './services/guards/role.guard';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'admin', component: AdminComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } },
  {path: 'admin/:admin', component: AdminComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' } },
  {path: 'meeting', component: MeetingComponent},
  {path: 'meeting/:meeting', component: MeetingComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'meeting/detail/:id', component: CardComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MeetingComponent,
    CardComponent,
    AdminComponent,
    LoginComponent,
    RegisterComponent,
    PaginationComponent,
    ModelComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [AuthService, MeetingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
