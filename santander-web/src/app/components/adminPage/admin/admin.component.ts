import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../services/meetingPage/admin.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./../../../app.component.css']
})
export class AdminComponent implements OnInit {

  public response: any;
  public totalPages: number;
  constructor(public adminService: AdminService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.activatedRoute.paramMap.subscribe(params => {
      let pageNumber: number = +params.get('admin');
      if (pageNumber === undefined || pageNumber === null) {
        pageNumber = 1;
      }
      this.getPage(pageNumber);
    });
  }

  getPage(pageNumber) {
    this.adminService.getMyMeetings(pageNumber - 1, 5).subscribe(response => {
      this.response = response;
      this.totalPages = response.totalPages;
    });
  }
}
