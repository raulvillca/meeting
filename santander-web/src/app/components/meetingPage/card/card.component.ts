import { Component, OnInit, Input } from '@angular/core';
import { Meeting } from '../../../models/meeting';
import { MeetingService } from '../../../services/meetingPage/meeting.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html'
})
export class CardComponent implements OnInit {
  
  @Input() meeting: Meeting = null;
  private id = this.route.snapshot.paramMap.get('id');
  constructor(public meetingService: MeetingService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.meetingService.getMeeting(this.id).subscribe(response => {
      this.meeting = response;
    });
  }

  joinMe() {
    this.meetingService.joinMe(this.id).subscribe();
  }

  chechIn() {
    this.meetingService.doCheckin(this.id).subscribe();
  }

  count = function (meeting: Meeting) {
    return this.meeting.attendees > 0 ? meeting.attendees.length : 0;
  }
}
