import { Component, OnInit } from '@angular/core';
import { MeetingService } from '../../../services/meetingPage/meeting.service';
import { Meeting } from '../../../models/meeting';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./../../../app.component.css']
})
export class MeetingComponent implements OnInit {

  public response: any;
  totalPages: any;
  constructor(public meetingService: MeetingService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.activatedRoute.paramMap.subscribe(params => {
      let pageNumber: number = +params.get('meeting');
      if (pageNumber == 0) {
        pageNumber = 1;
      }
      this.getMeetings(pageNumber);
    });
    
  }

  getMeetings(pageNumber: number) {
    this.meetingService.getMeetings(pageNumber - 1, 5).subscribe(response => {
      this.response = response;
      this.totalPages = response.totalPages;
    });
  }
}
