import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/loginPage/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {

  public username: string;
  public password: string;
  public mail: string;

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  register() : void {
    this.authService.register({'username': this.username, 'password': this.password, 'email': this.mail}).subscribe(response => {
      Swal.fire('Usuario Registrado', `${response.username} se registro con exito`, 'success');
      this.router.navigate(['/login']);
    }, err => {
      Swal.fire('Error al registrar', 'Fallo la autenticacion', 'error');
    });
  }

}
