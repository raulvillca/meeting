import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/loginPage/auth.service';
import { Router } from '@angular/router';
import { User } from '../../../models/user';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;
  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      Swal.fire('Sesion Iniciada', `Hola, ya estás autenticado!`, 'info');
      this.router.navigate(['/clientes']);
    }
  }

  login(): void {
    if(this.username == null || this.password == null) {
      Swal.fire('Error login', 'Error username o password', 'error');
      return;
    }
    this.authService.login(this.username, this.password).subscribe(response => {
      this.authService.saveToken(response.access_token);
      this.authService.saveUser(response.refresh_token);
      Swal.fire('Sesion Iniciada', `Hola ${this.username} inicio sesión con éxito!`, 'success');
      this.router.navigate(['/meeting']);
    }, err => {
      if (err.status == 400) {
        Swal.fire('Error en la sesion', 'Fallo la autenticacion', 'error');
      }
    });
  }
}
