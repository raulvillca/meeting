import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from '../../../services/meetingPage/admin.service';
import { Place } from '../../../models/place';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html'
})
export class ModelComponent implements OnInit {

  public placeId: number;
  public date: Date;
  public title: string;
  public places: Place[];

  @Input() pagination: any;

  constructor(public adminService: AdminService) { }

  ngOnInit() {
    this.adminService.getPlaces().subscribe(response => {
      this.places = response;
    });
  }

  createMeetup() {
    this.adminService.creatMeeting({date: this.date, placeId: this.placeId, title: this.title}).subscribe(response => {
      this.pagination.content.push(response);
    });
  }
}
