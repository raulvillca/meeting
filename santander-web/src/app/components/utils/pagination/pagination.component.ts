import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../../services/loginPage/auth.service';
import { ActivatedRoute } from '@angular/router';
import { MeetingService } from '../../../services/meetingPage/meeting.service';
import { Temp } from '../../../models/temp';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./../../../app.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() pagination: any;
  @Input() customRouterLink: any;

  public pages: number[];
  public weather: Temp;

  constructor(public authService: AuthService, public meetingService: MeetingService, private activateRoute: ActivatedRoute) { }

  ngOnInit() {
    this.pages = new Array(this.pagination.totalPages).fill(0).map((_valor, indice) => indice);
  }

  getWeather(meeting_id: number) {
    this.meetingService.getWeather(meeting_id).subscribe(response => {
      this.weather = response.body;
    });
  }

}
