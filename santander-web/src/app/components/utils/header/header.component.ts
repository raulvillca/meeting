import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/loginPage/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./../../../app.component.css']
})
export class HeaderComponent {

  constructor(public authService: AuthService, private router: Router) { }
  logout(): void {
    let username = this.authService.user.username;
    this.authService.logout();
    Swal.fire('Sesion Cerrada', `Hola ${username}, has cerrado sesión con éxito!`, 'success');
    this.router.navigate(['/login']);
  }

}
