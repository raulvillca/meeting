import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Meeting } from '../../models/meeting';
import { Observable, throwError } from 'rxjs';
import { URL_MEETING_CREATE, URL_MEETING_MY_MEETINGS, URL_MEETING_ALL_PLACES, URL_MEETING_ALL_BEERS } from '../../urls/url';
import { catchError, map } from 'rxjs/operators';
import { Place } from '../../models/place';
import { Beer } from '../../models/beer';
import { CustomHeader } from '../../custom-header';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private headers = new HttpHeaders();

  constructor(private http: HttpClient, private router: Router) { }

  creatMeeting(meetingDto: any): Observable<Meeting> {
    this.headers = CustomHeader.build();
    return this.http.post<Meeting>(URL_MEETING_CREATE, meetingDto, {headers: this.headers}).pipe(
      catchError(e => {
        this.router.navigate(['/meeting']);
        console.log(e);
        //Swal('Error al crear el evento', e, 'error');
        return throwError(e);
      })
    )
  }

  getMyMeetings(pageNumber: number, size: number): Observable<any> {
    this.headers = CustomHeader.build();
    return this.http.get(URL_MEETING_MY_MEETINGS + '?page=' + pageNumber+ '&size=' + size, {headers:this.headers}).pipe(
      map(response => response as Meeting[])
    );
  }

  getPlaces(): Observable<Place[]> {
    this.headers = CustomHeader.build();
    return this.http.get(URL_MEETING_ALL_PLACES, {headers:this.headers}).pipe(
      map((response: Place[]) => {
        (response).map(place => {
          place.place = place.place.toUpperCase();
          
          return place;
        });
        return response;
      })
    );
  }

  getBeers(): Observable<Beer[]> {
    this.headers = CustomHeader.build();
    return this.http.get(URL_MEETING_ALL_BEERS, {headers:this.headers}).pipe(
      map((response: Place[]) => {
        (response).map(place => {
          place.place = place.place.toUpperCase();
          
          return place;
        });
        return response;
      })
    );
  }
}
