import { Injectable } from '@angular/core';
import { Meeting } from '../../models/meeting';
import { URL_MEETING, URL_MEETING_CHECKIN, URL_MEETING_ALL_MEETINGS, URL_MEETING_CREATE, URL_MEETING_MY_MEETINGS, URL_MEETING_MY_MEETING, URL_MEETING_JOIN, URL_MEETING_ALL_PLACES, URL_MEETING_ALL_BEERS, URL_TEMP_GET_WEATHER } from '../../urls/url';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { of, Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Place } from '../../models/place';
import { Beer } from '../../models/beer';
import { CustomHeader } from '../../custom-header';
import { Temp } from '../../models/temp';

@Injectable()
export class MeetingService {
  private headers = new HttpHeaders();

  constructor(private http: HttpClient, private router: Router) { }

  getMeetings(pageNumber: number, size: number): Observable<any> {
    this.headers = CustomHeader.build();
    return this.http.get(URL_MEETING_ALL_MEETINGS + '?page=' + pageNumber+ '&size=' + size, {headers:this.headers}).pipe(
      map((response: any) => {
        (response.content as Meeting[]).map(meeting => {
          meeting.place.place = meeting.place.place.toUpperCase();
          
          return meeting;
        });
        return response;
      })
    );
  }

  getMeeting(meeting_id: any): Observable<Meeting> {
    this.headers = CustomHeader.build();
    return this.http.get<Meeting>(`${URL_MEETING_MY_MEETING}${meeting_id}`, {headers: this.headers}).pipe(
      map(response => response as Meeting),
      catchError(e => {
        this.router.navigate(['/meeting']);
        console.log(e);
        return throwError(e);
      })
    )
  }

  doCheckin(id): Observable<any> {
    this.headers = CustomHeader.build();
    return this.http.put<any>(`${URL_MEETING_CHECKIN}${id}`, {headers:this.headers}).pipe(
      catchError(e => {
        console.log(e);
        return throwError(e);
      })
    );
  }

  joinMe(id): Observable<any> {
    this.headers = CustomHeader.build();
    return this.http.put<any>(`${URL_MEETING_JOIN}${id}`, {headers:this.headers}).pipe(
      catchError(e => {
        console.log(e);
        return throwError(e);
      })
    );
  }

  getWeather(id): Observable<any> {
    this.headers = CustomHeader.build();
    return this.http.get<any>(`${URL_TEMP_GET_WEATHER}${id}`, {headers:this.headers}).pipe(
      catchError(e => {
        console.log(e);
        return throwError(e);
      })
    );
  }
}
