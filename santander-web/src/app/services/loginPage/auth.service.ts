import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user';
import { URL_LOGIN, URL_REGISTER } from '../../urls/url';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _TOKEN_ : string;
  private _USER_ : User;
  private headers = new HttpHeaders({
    'Content-type':'application/x-www-form-urlencoded', 
    'Authorization': 'Basic ' + btoa('santander'+':'+'JWTSantanderSecretKey')
  });

  constructor(private http: HttpClient) { }

  login(username: string, password: string) : Observable<any> {
    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', username);
    params.set('password', password);
    return this.http.post(URL_LOGIN, params.toString(), {headers: this.headers});
  } 

  register(userDto: any) : Observable<any> {
    let customHeaders = new HttpHeaders({
      'Content-type':'application/json', 
      'Authorization': 'Basic ' + btoa('santander'+':'+'JWTSantanderSecretKey')
    });
    return this.http.post(URL_REGISTER, userDto, {headers: customHeaders});
  }

  getCredentials(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(atob(accessToken.split(".")[1]));
    }
    return null;
  }

  saveUser(accessToken: string): void {
    let payload = this.getCredentials(accessToken);
    this._USER_ = new User();
    this._USER_.username = payload.user_name;
    this._USER_.credentials = payload.authorities;
    sessionStorage.setItem('user', JSON.stringify(this._USER_));
  }

  saveToken(token: string): void {
    sessionStorage.setItem('token', token);
    this._TOKEN_ = token;
  }

  get(token: string): string {
    if(this._TOKEN_ != null) {
      return this._TOKEN_;
    } else {
      return sessionStorage.getItem('token');
    }
  }

  public get user(): User {
    if (this._USER_ != null) {
      return this._USER_;
    } else if (this._USER_ == null && sessionStorage.getItem('user') != null) {
      this._USER_ = JSON.parse(sessionStorage.getItem('user')) as User;
      return this._USER_;
    }
    return new User();
  }

  isAuthenticated(): boolean {
    let payload = this.getCredentials(this._TOKEN_);
    if (payload != null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }
    return false;
  }

  hasRole(role: string): boolean {
    if (this._USER_ != null && this._USER_.credentials.includes(role)) {
      return true;
    }
    return false;
  }

  logout(): void {
    this._TOKEN_ = null;
    this._USER_ = null;
    sessionStorage.clear();
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
  }
}
